function factorial_rec(input) {
    if (input < 0) return;
    if (input === 0) return 1;
    return input * factorial_rec(input - 1);
}

function factorial_iter(input) {
    if (input < 0) return;
    if (input === 0) return 1;
    var result = input;
    for (var i = 1; i < input; i++) {
        result *= i;
    }
    return result;
}

var number = 5;

console.log('Factorial(recursive) of ' + number + ' is: ' + factorial_rec(number));
console.log('Factorial(iteration) of ' + number + ' is: ' + factorial_iter(number));