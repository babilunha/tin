function fibonacci(input){
    var a = 1, b = 0, temp;
    while (input >= 0){
      temp = a;
      a = a + b;
      b = temp;
      input--;
    }
    return b;
  }

  var number = 5;

console.log('fibonacci number with index ' + number + ' is: ' + fibonacci(number));