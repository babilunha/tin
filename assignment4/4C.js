function palindrome(input) {
    var regex = /[\W_]/g;
    var lowerCase = input.toString().toLowerCase();
    var onlyLetters = lowerCase.replace(regex, '');
    var reverseStr = onlyLetters.split('').reverse().join(''); 
    return reverseStr === onlyLetters;
  }

  var input = 'ak';
  console.log(input + '\nis a palindrome? ' + palindrome(input));