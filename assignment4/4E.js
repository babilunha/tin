/*
i could have removed anything except characters from a to z (just like in the one of the previous tasks),
but i thought that it won't fulfill the requirements then
*/
function findLongestWord(input) {
    if (typeof input !== 'string') return "no result (input is not a string)";
    var longestWord = input.split(' ').sort(function(a, b) { return b.length - a.length; });
    return longestWord[0];
}

var input = 123123;
console.log(input + '\nlongest is : ' + findLongestWord(input));