function returnType(input) {
    return typeof input;
}

var input = 1123;
console.log(input + '\ninput is of type: ' + returnType(input));