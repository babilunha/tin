function secondLowestGreatest(input) {
    if (!Array.isArray(input)) return "no result (input is not a array)";
    var filtered =  input.filter(function(a) { return typeof a === 'number'; });
    var sorted = filtered.sort(function(a, b) { return a - b; });
    var result = [sorted[1], sorted[sorted.length - 2]];
    return result;
}


var input = ['aaa',5,2,6,7,8,9,4,1];
console.log(input + '\nsecond lowest and greatest are: ' + secondLowestGreatest(input));