function amountTocoins(amount, coins) {
    if (!Array.isArray(coins)) return "no result (coins variable has to be an array)";
    if(typeof amount !== 'number') return "no result (amount variable has to a number)";
    var filteredCoins =  coins.filter(function(a) { return typeof a === 'number'; });
 if (amount === 0) {
     return [];
   } else {
     if (amount >= filteredCoins[0]) {
        left = (amount - filteredCoins[0]);
        return [filteredCoins[0]].concat( amountTocoins(left, filteredCoins) );
        } else {
            filteredCoins.shift();
         return amountTocoins(amount, filteredCoins);
        }
    }
} 


var money = 456;
var coins = ['aaaa', '100', 25, 10, 5, 2,1]
console.log(money + ' ' + coins + '\namount in coins is : ' + amountTocoins(money, coins));