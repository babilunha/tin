function binary_search(array, target) {
    
    var left = 0,
        right = array.length - 1;
    
    while (left < right) {
        var middle = Math.floor((left + right)/2);
        if(array[middle] < target) left = middle + 1;
        else if (array[middle] > target) right = middle - 1;
        else return middle;
    }

}

var input = [0,10,20,30,40,50,60,70,80,90,100];
var target = 50;
console.log(input + '\nindex of ' + target + ' is: ' + binary_search(input, target));