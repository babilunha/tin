/*for all of the tasks I didn't make any type or format checking, It's really not a problem to do them but it does take quite a lot of time. 
just say and I'll do them (or actually I'll just ask you about it on thursday, when we'll have classes)*/

var person = { name: 'maks',
                surname: 'babilunha',
                age: 19,

                fullName: function() {
                    return this.name + ' ' + this.surname;
                },
                
                dateOfBirth: function() {
                    return (new Date()).getFullYear() - this.age;
                }};

function printObject(inObject) {
    for (var i in inObject) {
        console.log(i + " " + typeof inObject[i]);
    }
}

console.log(person.fullName());
console.log(person.dateOfBirth());
printObject(person);