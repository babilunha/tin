/*for all of the tasks I didn't make any type or format checking, It's really not a problem to do them but it does take quite a lot of time. 
just say and I'll do them (or actually I'll just ask you about it on thursday, when we'll have classes)*/

var id = 0;
function Student(first, last, inGrades) {
    this.id = ++id;
    this.firstName = first;
    this.lastName = last;
    this.grades = inGrades;
    
    this.fullInfo = function() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: " + this.id + " name: " + this.firstName + " " + this.lastName + " avg: " + avg;
    };
}


var studentMaks = new Student("Maks", "Babilunha", [4, 5, 4, 5]);



console.log(studentMaks.fullInfo());
