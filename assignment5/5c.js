/*for all of the tasks I didn't make any type or format checking, It's really not a problem to do them but it does take quite a lot of time. 
just say and I'll do them (or actually I'll just ask you about it on thursday, when we'll have classes)*/


var studentBase = { 
    obligatoryCourses: ['english', 'math', 'biology', 'geography']
};


function createStudent(_id, _name, _surname) { 
    var student = Object.create(studentBase);
    student.id = _id;
    student.name = _name;
    student.surname = _surname;
    return student;
}



var st1 = createStudent(1, 'Freddie', 'Mercury');

console.log(st1);
console.log(st1.obligatoryCourses);