/*for all of the tasks I didn't make any type or format checking, It's really not a problem to do them but it does take quite a lot of time. 
just say and I'll do them (or actually I'll just ask you about it on thursday, when we'll have classes)*/

class Student {
    constructor(id, name, surname, grades) {
        
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.grades = grades;

    }

    fullInfo() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: " + this.id + " name: " + this.name + " " + this.surname + " avg: " + avg;
    };

    get fullName() {
        return this.name + ' ' + this.surname;
    }

    set fullName(name) {
        var words = name.split(' ');
        this.name = words[0] || '';
        this.surname = words[1] || '';
    }

    get AvgGrade() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return avg;
    }
}

var student1 = new Student(1, 'dante', 'alighieri', [4,5,5,5,5]);

console.log(student1.fullInfo());
student1.fullName = 'frederic chopin';
console.log(student1.fullName);
console.log(student1.AvgGrade);