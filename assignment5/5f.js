/*for all of the tasks I didn't make any type or format checking, It's really not a problem to do them but it does take quite a lot of time. 
just say and I'll do them (or actually I'll just ask you about it on thursday, when we'll have classes)*/

class Person {
    constructor(name, surname) {
        this.name = name;
        this.surname = surname;
    }

    get fullName() {
        return this.name + ' ' + this.surname;
    }

    set fullName(name) {
        var words = name.split(' ');
        this.name = words[0] || '';
        this.surname = words[1] || '';
    }
}

class Student extends Person {
    constructor(name, surname, id, grades) {
        super(name, surname);
        this.id = id;
        this.grades = grades;
      }

      
    fullInfo() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return "id: " + this.id + " name: " + this.name + " " + this.surname + " avg: " + avg;
    };


    get AvgGrade() {
        var sum = 0;
        for (var i = 0; i < this.grades.length; i++) {
            sum += parseInt(this.grades[i], 10);
        }
        var avg = sum/this.grades.length;
        return avg;
    }
}

var person1 = new Person('post', 'malone');
var student1 = new Student('chet', 'baker', 1532, [5,5,5,5,4]);

console.log(person1);
person1.fullName = 'michael jackson';
console.log(person1);
console.log(person1.fullName);
console.log(student1.fullInfo());
console.log(student1.AvgGrade)


