function myFunction() {
    
    var email = document.getElementById("f1").value;
    var number = document.getElementById("f2").value;

    if (!validateEmail(email)) displayEmailError();
    else displayEmailConfirmation();
    if (!validateNumber(number)) displayNumberError();
    else displayNumberConfirmation();

     
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validateNumber(number) {
    var re = /^\d+$/;
    return re.test(String(number).toLowerCase());
}

function displayEmailError() {
    
    document.getElementById("f1").style.color = "red";
    document.getElementById("f1").style.borderColor = "red";
}

function displayNumberError() {
    document.getElementById("f2").style.color = "red";
    document.getElementById("f2").style.borderColor = "red";
}

function displayEmailConfirmation() {
    document.getElementById("f1").style.color = "green";
    document.getElementById("f1").style.borderColor = "green";
    
}

function displayNumberConfirmation() {
    document.getElementById("f2").style.color = "green";
    document.getElementById("f2").style.borderColor = "green";
    
}

function resetFunction() {
    resetColor();
    document.getElementById("f1").value = "";
    document.getElementById("f2").value = "";

}

function resetColor() {
    document.getElementById("f1").style.color = "black";
    document.getElementById("f2").style.color = "black";
    document.getElementById("f1").style.borderColor = "black";
    document.getElementById("f2").style.borderColor = "black";
}