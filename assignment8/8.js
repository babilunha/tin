const express = require('express');
const app = express();
var bodyParser = require("body-parser");
let ejs = require('ejs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('view engine', 'ejs');


const port = 8080;

/*
* handling /hello route 
*/
app.get('/hello', function (req, res) {
  res.render("hello");
});


/*
* handling /form route 
*/
app.get('/form', function (req, res) {
  res.render("form");
});

app.post('/formdata', function (req, res) {

  var v1 = req.body.tf1;
  var v2 = req.body.tf2;
  var v3 = req.body.tf3;

  var data = {
    value1: v1,
    value2: v2,
    value3: v3
  };

  res.render("formdata", data);
});

app.post('/jsondata', function (req, res) {

  res.render("formdata", req.body);

});







//listening
app.listen(port, () => console.log(`listening on port ${port}`));