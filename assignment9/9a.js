const express = require('express');
const app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
const port = 8080;

app.use((req, res, next) => {

    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

app.post('/server', function (req, res) {

    number1 = req.body.value1;
    number2 = req.body.value2;
    operation = req.body.value3;



    var my_result = calculate(number1, number2, operation);

    // res.sendStatus(200).send(JSON.stringify(my_result));

    res.send(JSON.stringify({ result: my_result }));
});

function calculate(n1, n2, op) {

    n1 = parseInt(n1);
    n2 = parseInt(n2);

    switch (op) {
        case 'add':
            return n1 + n2;

        case 'sub':
            return n1 - n2;

        case 'div':
            return n1 / n2;

        case 'mul':
            return n1 * n2;

        default:
            return 0;
    }

}

//listening
app.listen(port, () => console.log(`listening on port ${port}`));