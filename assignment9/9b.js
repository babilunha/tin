function loadXMLDoc() {
    
    var xhttp = new XMLHttpRequest();

    var oper_sel = document.getElementById("operation");
    var my_option = oper_sel.options[oper_sel.selectedIndex].value;

    var object_to_send = {
        value1: document.getElementById("tf1").value,
        value2: document.getElementById("tf2").value,
        value3: my_option
    };

    xhttp.open("post", "http://localhost:8080/server", true);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("answer").innerHTML = JSON.parse(xhttp.responseText).result;
        }
    };
    xhttp.send(JSON.stringify(object_to_send));
}
